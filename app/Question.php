<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable =['text','a','b','c','d','answer','module_id']; //coge lo del formulario, si esta lo mete sino no


    public function exams(){
        return  $this->belongsToMany(Exam::class);
    }

    public function module(){
        return  $this->belongsTo(Module::class);
    }
}
