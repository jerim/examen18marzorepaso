<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{

  protected $fillable =['title','module_id','user_id','date']; //coge lo del formulario, si esta lo mete sino no

  public function user(){
      return  $this->belongsTo(User::class);

    }


  public function module(){
         return  $this->belongsTo(Module::class);

    }

  public function questions(){
         return  $this->belongsToMany(Question::class);

   }


}

