<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Module;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $question=Question::all();
        return view('questions.index',['question'=>$question]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $question=Question::all();
        $module= Module::all();
        return view('questions.create',['question'=>$question],['module'=>$module]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $rules=[
            'text' => 'unique:questions,text|required|max:255' ,
            'a' =>'unique:questions,a|required|max:255',
            'b' =>'unique:questions,b|required|max:255',
            'c' =>'unique:questions,c|required|max:255',
            'd' =>'unique:questions,d|required|max:255',
            'answer' =>'unique:questions,answer|required|max:255',
            'module_id' => 'unique:questions,module_id|exists:modules,id' ,
        ];

        $request->validate($rules);



        $question = new Question;
        $question->fill($request->all());
        $question->save();
        return redirect('/questions');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $question=Question::find($id);
        return view('questions.show',['question'=>$question]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question=Question::find($id);
        $module= Module::all();
        return view('questions.edit',['question'=>$question,'module'=>$module]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

         $rules=[
            'text' => 'unique:questions,text|required|max:255' ,
            'a' =>'unique:questions,a|required|max:255',
            'b' =>'unique:questions,b|required|max:255',
            'c' =>'unique:questions,c|required|max:255',
            'd' =>'unique:questions,d|required|max:255',
            'answer' =>'unique:questions,answer|required|max:255',
            'module_id' => 'unique:questions,module_id|exists:modules,id' ,
        ];

        $request->validate($rules);


        $question=Question::find($id); //Busca el id del modulo ya creado
        $question->fill($request->all());
        $question->save();
        return redirect('/questions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Question::destroy($id);
        return back();
    }
}
