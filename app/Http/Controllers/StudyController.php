<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Study;
use App\Family;
use App\Module;
class StudyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $study=Study::all();
        return view('studies.index',['study'=>$study]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $study=Study::all();
        $family=Family::all();
        return view('studies.create',['study'=>$study,'family'=>$family]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $rules=[
           'code' => 'unique:studies,code|required|max:255' ,
           'name' =>'unique:studies,name|required|max:255',
       ];

       $request->validate($rules);


       $study = new Study;
       $study->fill($request->all());
       $study->save();
       return redirect('/studies');
   }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $study=Study::find($id);
        $modules=Module::all();
        return view('studies.show',['study'=>$study,'modules'=>$modules]);

    }

    public function attachModule(Request $request,$id)
    {
        $study=Study::find($id);
        $module_id=$request->input('module_id');
        $course=$request->input('course');
        // dd($course);
        $study->modules()->syncWithoutDetaching([$module_id => ['course'=>$course]]);
        $rules=[
          'module_id' => 'required|max:255' ,
           'course' =>'required|numeric|max:2',
        ];
        $messages = [
            'required' => 'The :attribute field is required.',
            'max' => 'The :attribute numeric is  max :2 ',
        ];
        $request->validate($rules,$messages);
        return back();

    }
    public function detachModule(Request $request, $id)
    {
     $study=Study::find($id);
     $module_id=$request->input('module_id');
     $study->modules()->detach($module_id);
     return back();
 }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $study=Study::find($id);
        $family=Family::all();
        return view('studies.edit',['study'=>$study,'family'=>$family]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $rules=[
           'code' => 'unique:studies,code|required|max:255' ,
           'name' =>'unique:studies,name|required|max:255',
       ];

       $request->validate($rules);


       $study = Study::find($id);
       $study->fill($request->all());
       $study->save();
       return redirect('/studies');
   }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Study::destroy($id);
        return back();
    }


}
