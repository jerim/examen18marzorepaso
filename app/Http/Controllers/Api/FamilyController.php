<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Family;

class FamilyController extends Controller
{
    public function index()
    {
        return Family::all();

    }
    public function store(Request $request)
    {

     $rules=[
       'code' => 'unique:families,code|required|max:255' ,
       'name' =>'unique:families,name|required|max:255',
    ];

     $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            // return $validator->errors(); //status 200, mal
            //mejor así:
            return response()->json($validator->errors(), 400);

      }

      $family=new Family;
      $family->fill($request->all());
      $family->save();

      return $family;


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $family =  Family::all()->find($id);
        if($family){
            return $family;
        }else{
        return response()->json([
                'message'=>'Record not found',
        ],404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $rules=[
        'code' => 'unique:modules,code|required|max:255' ,
        'name' =>'unique:modules,name|required|max:255',
    ];

    $validator = Validator::make($request->all(), $rules);

    if ($validator->fails()) {
            // return $validator->errors(); //status 200, mal
            //mejor así:
        return response()->json($validator->errors(), 400);

    }

    $family =  Family::find($id);

    if($family){

       $family->fill($request->all());
       $family->save();
       $family->refresh();
       return $family;

   }else{

    return response()->json([
        'message'=>'Record not found',
    ],404);
}


}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         Family::destroy($id);
        return response()->json([
                'message'=>'Delete',
        ],201);
    }
}
