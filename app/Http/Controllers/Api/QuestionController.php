<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Exam;
use App\Question;
use App\Module;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          return Question::with('exams','module')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $rules=[
            'text' => 'unique:questions,text|required|max:255' ,
            'a' =>'unique:questions,a|required|max:255',
            'b' =>'unique:questions,b|required|max:255',
            'c' =>'unique:questions,c|required|max:255',
            'd' =>'unique:questions,d|required|max:255',
            'answer' =>'unique:questions,answer|required|max:255',
            'module_id' => 'unique:questions,module_id|exists:modules,id' ,
      ];

      $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            // return $validator->errors(); //status 200, mal
            //mejor así:
            return response()->json($validator->errors(), 400);

       }

      $question= new Question;
      $question->fill($request->all());
      $question->save();

      return $question;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $question =  Question::with('text','a','b','c','d','answer','module_id')->find($id);
        if($question){
            return $question;
        }else{
        return response()->json([
                'message'=>'Record not found',
        ],404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

       $rules=[
            'text' => 'unique:questions,text|required|max:255' ,
            'a' =>'unique:questions,a|required|max:255',
            'b' =>'unique:questions,b|required|max:255',
            'c' =>'unique:questions,c|required|max:255',
            'd' =>'unique:questions,d|required|max:255',
            'answer' =>'unique:questions,answer|required|max:255',
            'module_id' => 'unique:questions,module_id|exists:modules,id' ,
      ];

      $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            // return $validator->errors(); //status 200, mal
            //mejor así:
            return response()->json($validator->errors(), 400);

       }


        $question =  Question::find($id);
        if($question){

            $question->fill($request->all());
            $question->save();
            $question->refresh();
                 return $question;
        }else{

        return response()->json([
                'message'=>'Record not found',
        ],404);
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Question::destroy($id);
        return response()->json([
                'message'=>'Delete',
        ],201);
    }
}
