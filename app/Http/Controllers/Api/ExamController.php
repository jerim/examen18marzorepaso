<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Exam;
class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return User::all();
         return Exam::with('user')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $rules = [
            'title' => 'required|max:255',
            'module_id' => 'exists:modules,id',
            'user_id' => 'exists:users,id',
            'date' => 'required|date',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            // return $validator->errors(); //status 200, mal
            //mejor así:
            return response()->json($validator->errors(), 400);

        }

         // return "guardar";
       //necesita el atributo fillable en el modelo | opcion 1
      $exam = new Exam;
      $exam->fill($request->all());
      $exam->save();

      return $exam;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $exam =  Exam::with('user','module','questions')->find($id);
        if($exam){
            return $exam;
        }else{
        return response()->json([
                'message'=>'Record not found',
        ],404);
        }
}
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

         $rules = [
            'title' => 'required|max:255',
            'module_id' => 'exists:modules,id',
            'user_id' => 'exists:users,id',
            'date' => 'required|date',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            // return $validator->errors(); //status 200, mal
            //mejor así:
            return response()->json($validator->errors(), 400);

        }


        $exam = Exam::with('user', 'module')->find($id);

        if (!$exam) {
            return response()->json([
                'message' => 'Record not found',
            ], 404);
        }


        $exam->fill($request->all());
        $exam->save();
        $exam->refresh();
        return $exam;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Exam::destroy($id);
        return response()->json([
                'message'=>'Delete',
        ],201);
    }
}
