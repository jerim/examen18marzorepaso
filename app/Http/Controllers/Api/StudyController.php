<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Study;
class StudyController extends Controller
{
     public function index()
    {
       return Study::with('family')->get();



    }
    public function store(Request $request)
    {

         $rules=[
             'code' => 'unique:studies,code|required|max:255' ,
             'name' =>'unique:studies,name|required|max:255',
          ];

          $validator = Validator::make($request->all(), $rules);

          if ($validator->fails()) {
            // return $validator->errors(); //status 200, mal
            //mejor así:
            return response()->json($validator->errors(), 400);

        }

        $study=new Study;
        $study->fill($request->all());
        $study->save();

        return $study;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $study =  Study::with('family')->find($id);
        if($study){
            return $study;
        }else{
        return response()->json([
                'message'=>'Record not found',
        ],404);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rules=[
        'code' => 'unique:modules,code|required|max:255' ,
        'name' =>'unique:modules,name|required|max:255',
    ];

     $validator = Validator::make($request->all(), $rules);

     if ($validator->fails()) {
            // return $validator->errors(); //status 200, mal
            //mejor así:
        return response()->json($validator->errors(), 400);

        }

     $study =  Study::find($id);

        if($study){

      $study->fill($request->all());
      $study->save();
      $study->refresh();
     return $study;

 }else{

    return response()->json([
        'message'=>'Record not found',
    ],404);
}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         Study::destroy($id);
        return response()->json([
                'message'=>'Delete',
        ],201);
    }
}
