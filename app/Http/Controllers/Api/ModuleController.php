<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Exam;
use App\Module;
use App\Question;
class ModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         return Module::with('exams','questions')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $rules=[
            'code' => 'unique:modules,code|required|max:255' ,
            'name' =>'unique:modules,name|required|max:255',
      ];

      $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            // return $validator->errors(); //status 200, mal
            //mejor así:
            return response()->json($validator->errors(), 400);

      }

      $module = new Module;
      $module->fill($request->all());
      $module->save();

      return $module;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $module =  Module::with('code','name')->find($id);
        if($module){
            return $module;
        }else{
        return response()->json([
                'message'=>'Record not found',
        ],404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $rules=[
            'code' => 'unique:modules,code|required|max:255' ,
            'name' =>'unique:modules,name|required|max:255',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            // return $validator->errors(); //status 200, mal
            //mejor así:
            return response()->json($validator->errors(), 400);

      }




        $module =  Module::find($id);

        if($module){

           $module->fill($request->all());
           $module->save();
           $module->refresh();
             return $module;

        }else{

        return response()->json([
                'message'=>'Record not found',
        ],404);
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Module::destroy($id);
        return response()->json([
                'message'=>'Delete',
        ],201);
    }
}
