<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
class StudentController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth')->except('index');

    }

    public function index()
    {
        $student= Student::all();

        return view('students.index', ['student'=>$student]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $student=Student::all();
        return view('students.create', ['student'=>$student]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules=[
            'name' => 'unique:students,name|required|max:255' ,
            'surname' =>'unique:students,surname|required|max:255',
            'date'=>'unique:students,date|required|date',
            'address'=>'unique:students,address|required|max:255',
            'email'=>'unique:students,email|required|max:255',
        ];

        $request->validate($rules);

        $student= new Student;
        $student->fill($request->all());
        $student->save();

        return redirect('/students');



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student=Student::find($id);
        return view('students.show', ['student'=>$student]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student=Student::find($id);
        return view('students.edit', ['student'=>$student]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $rules=[
            'name' => 'unique:students,name|required|max:255' ,
            'surname' =>'unique:students,surname|required|max:255',
            'date'=>'unique:students,date|required|date',
            'address'=>'unique:students,address|required|max:255',
            'email'=>'unique:students,email|required|max:255',
        ];

        $request->validate($rules);

        $student= new Student;
        $student->fill($request->all());
        $student->save();

        return redirect('/students');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Student::destroy($id);
        return back();
    }
}
