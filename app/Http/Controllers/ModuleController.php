<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Module;
class ModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //$user=\Auth::user();
        //if($user->can('index',Module::class)){
           // return " you can ";

        //}else{
           // return "you do not can ";
       // }

        $this->authorize('index',Module::class);

        $module= Module::all();

        return view('modules.index', ['module'=>$module]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $module= Module::all();

        return view('modules.create', ['module'=>$module]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $rules=[
            'code' => 'unique:modules,code|required|max:255' ,
            'name' =>'unique:modules,name|required|max:255',
        ];

        $request->validate($rules);


        $module = new Module;
        $module->fill($request->all());
        $module->save();
        return redirect('/modules');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $module=Module::find($id);
        $this->authorize('view',$module);
        return view('modules.show', ['module'=>$module]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $module=Module::find($id);
        return view('modules.edit', ['module'=>$module]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rules=[
            'code' => 'unique:modules,code|required|max:255' ,
            'name' =>'unique:modules,name|required|max:255',
        ];

        $request->validate($rules);


        $module=Module::find($id); //Busca el id del modulo ya creado
        $module->fill($request->all());
        $module->save();
        return redirect('/modules');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Module::destroy($id);

        return back(); //vuelva a la pag
    }
}
