<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Family;

class FamilyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $family=Family::all();
        return view('families.index',['family'=>$family]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $family=Family::all();
        return view('families.create',['family'=>$family]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
         $rules=[
             'code' => 'unique:families,code|required|max:255' ,
             'name' =>'unique:families,name|required|max:255',
             ];

         $request->validate($rules);


         $family = new Family;
         $family->fill($request->all());
         $family->save();
         return redirect('/families');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $family=Family::find($id);
        return view('families.show',['family'=>$family]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $family=Family::find($id);
        return view('families.edit',['family'=>$family]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules=[
             'code' => 'unique:families,code|required|max:255' ,
             'name' =>'unique:families,name|required|max:255',
        ];

        $request->validate($rules);


        $family = Family::find($id);
        $family->fill($request->all());
        $family->save();
        return redirect('/families');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Family::destroy($id);

        return back(); //vuelva a la pag
    }
}
