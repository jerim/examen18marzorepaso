<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exam;
use App\Module;
use App\User;
use App\Question;
use Session;
use Auth;
use DB;
class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth')->except('index');
    }

    public function index(){
        $exam= Exam::all();

        return view('exams.index', ['exam'=>$exam]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){

        $module=Module::all();
        $user=User::all();
        return view('exams.create',['module'=>$module,'user'=>$user]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

     $rules=[
        'title' => 'unique:exams,title|required|max:255'  ,
        'module_id' => 'exists:modules,id' ,
            //'user_id' =>' exists:users,id',
        'date' => 'unique:exams,date|required|date',
    ];

     $request->validate($rules);


       // return "guardar";
       //necesita el atributo fillable en el modelo | opcion 1
      $exam = new Exam;
      $exam->fill($request->all());
      $exam->user_id = \Auth::user()->id; //busca el auth, la function user busca su id
      $exam->save();

       //opcion 2
        //$exams = App\Exam:::create($request->all())


      return redirect('/exams');

  }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $exam=Exam::find($id);
        return view('exams.show', ['exam'=>$exam]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){

        $exam= Exam::find($id);
        $user = User::all();
        $module= Module::all();
        return view('exams.edit',['exam'=>$exam, 'module'=>$module,'user'=>$user]);
        //return "page edit ";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $rules=[
            'title' => 'unique:exams,title|required|max:255' ,
            'module_id' => 'exists:modules,id' ,
            //'user_id' =>' exists:users,id',
            'date' => 'unique:exams,date|required|date',
        ];

        $request->validate($rules);

        $exam =Exam::find($id);
        $exam->fill($request->all());
        $exam->user_id = \Auth::user()->id; //busca el auth, la function user busca su id
        $exam->save();
        return redirect('/exams');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



public function remember($id,Request $request){ //request para la session
    $exam=Exam::find($id);

    $request->session()->put('exam', $exam);

    return back();
}



public function new(Request $request){ //seleccionar questions de cada module
    if(Session::has('module')){
        $module = Session::get('module');
        $questions = Question::where('module_id',$module->id)->get();
        $request->session()->put('module', $module);
        return view('exams.chooseQuestions',[ 'module' => $module,'questions'=>$questions]);

    } else {
        $modules = Module::all();
        return view('exams.back', ['modules' => $modules]);
    }

}

public function setModule(Request $request){
    $module= Module::find($request->module_id);
    Session::put('module', $module);
    Session::put('title', $request->title);
    Session::put('date', $request->date);
    return redirect('/exams/new');
}

/* public function chooseQuestions(Request $request){

        $questions=Question::all();

        return view('exams.chooseQuestions',['questions'=>$questions]);
    }*/


public function back(Request $request){ // seleccionar modules con select

    $modules = Module::all();
    return view('exams.back', ['modules' => $modules]);
}

//SWITCHQuestion
//BUSCAR LA PREGUNTA, SI EXISTE LA QUITO SINO LA PONGO
//se guarda con put y se devuelve
public function plus($id){ //añadir question a la session (+)
    $question=Question::find($id);
    $questions=Session::get('questions');

    if(isset($questions[$question->id])){
      unset($questions[$id]);
  } else {
   $questions[$id]=$question;

}

Session::put('questions',$questions);

return back();
}

public function minus(Request $request){ //eliminar question de la session (-)
    $request->session()->forget('questions');
    return back();
}

public function trash(Request $request){ //resetear

    Session::forget('module');
    Session::forget('title');
    Session::forget('date');
    Session::forget('questions');
    return redirect('/exams/new');
}


public function forget(Request $request){

    $request->session()->forget('exam');
    return back();

}

public function save(){
    DB::beginTransaction();
    // try {
        $exam = new Exam;
        $exam->user_id = Auth::user()->id;
        $exam->module_id = Session::get('module')->id;
        $exam->title = Session::get('title');
        $exam->date = Session::get('date');
        $exam->save();
        foreach (Session::get('questions') as $question) {
            $exam->questions()->attach($question->id);
        }

        // $exam->questions()->attach(985989898);
//
        //forget
        Session::forget('module');
        Session::forget('title');
        Session::forget('date');
        Session::forget('questions');
        DB::commit();
        return redirect ('/exams/' . $exam->id);
    // } catch (QueryException $e) {
    //     // DB::rollback();
    //     $request->session()->flash('status', 'El alta falla!');
        // return back();
    // }
}



public function destroy($id)
{
    $exam = Exam::find($id);

    $this->authorize('delete', $exam);

    $exam->delete();
    return back();
}


}

