<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Family extends Model
{

    protected $fillable =['code','name'];

    public function studies(){
        return $this->hasMany(Study::class);
    }

}
