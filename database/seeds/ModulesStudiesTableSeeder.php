<?php

use Illuminate\Database\Seeder;

class ModulesStudiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $course = 1;
        $study=\App\Study::find(1);
        $study->modules()->attach(1, ['course' => $course]);

        $study=\App\Study::find(1);
        $study->modules()->attach(2, ['course' => $course]);

        $study=\App\Study::find(1);
        $study->modules()->attach(3, ['course' => $course]);


    }
}

