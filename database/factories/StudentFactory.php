<?php

use Faker\Generator as Faker;



$factory->define(App\Student::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'surname' => $faker->lastName,
        'date' => $faker->date($format = 'Y-m-d', $min = '2017-09-01', $max = '2018-06-01'),
        'address' => $faker->address,
        'email' => $faker->unique()->safeEmail,

    ];
});
