@extends('layouts.app')

@section('content')
    <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                        <h1>Create User</h1>
                            <form class="form"  method="post" action="/users">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label>Name</label>
                                    <input class="form-control" type="text" name="name">
                                </div>

                                <div class="form-group">
                                    <label>Email</label>
                                    <input class="form-control" type="text" name="email">
                                </div>

                                 <input type="submit" value="New User" class="btn btn-success"  role="button">
                                 <a href="/users" class="btn btn-success"  role="button">Come back User's Home</a>
                         </form>
                </div>

            </div>
    </div>
@endsection


