@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Users</h1>
            <table  class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach( $user as $user )
                        <tr>
                            <td> {{$user->name}}  </td>
                            <td>{{ $user->email}}</td>
                            <td><a  href="/users/<?php echo $user->id ?>" class="btn btn-success"  role="button" >See</a></td>
                               <td><a  href="/users/<?php echo $user->id ?>/edit" class="btn btn-success"  role="button" >Edit</a></td>
                             <td>
                                <form method="post" action="/users/{{$user->id}}">
                                     {{ csrf_field() }}
                                     <input type="hidden" name="_method" value="delete">
                                     <input type="submit" value="Destroy" class="btn btn-danger"  role="button">
                              </form>
                             </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>

         </div>
    </div>
</div>
@endsection

