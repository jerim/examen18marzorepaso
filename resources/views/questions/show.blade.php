@extends('layouts.app')

@section('title', 'Question')

@section('content')
            <h1>
              Question's show's list <?php echo $question->id ?>
            </h1>

            <ul>
                <li>Question: {{$question->text}} </li>
                <li>A: {{$question->a}} </li>
                <li>B: {{$question->b}} </li>
                <li>C: {{$question->c}} </li>
                <li>D: {{$question->d}} </li>
                <li>Answer: {{$question->answer}} </li>
                <li>Module: {{$question->module->name}} </li>

            </ul>

            <h2>
              Exam's list <?php echo $question->id ?>
            </h2>

             <ul>
                @foreach($question->exams as $exam )
                <li> Exam: {{$exam->title}}</li>
                @endforeach
             </ul>


            <a href="/questions" class="btn btn-success"  role="button">Come back Question's Home</a>


@endsection

