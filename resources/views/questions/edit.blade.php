@extends('layouts.app')

@section('content')
    <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                        <h1>Edit Question</h1>
                            <form class="form"  method="post" action="/questions/{{$question->id}}">
                                {{ csrf_field() }}

                                 <input type="hidden" name="_method" value="put">

                                 <div class="form-group">
                                    <label>Question</label>
                                    <input class="form-control" type="text" name="text"
                                     value="{{$question->text}}">

                                       @if ($errors->first('text'))
                                        <div class="alert alert-danger ">
                                           {{$errors->first('text')}}
                                        </div>
                                      @endif


                                 </div>

                                 <div class="form-group">
                                    <label>A</label>
                                    <input class="form-control" type="text" name="a"
                                     value="{{$question->a}}">

                                      @if ($errors->first('a'))
                                        <div class="alert alert-danger ">
                                           {{$errors->first('a')}}
                                        </div>
                                      @endif

                                 </div>

                                 <div class="form-group">
                                     <label>B</label>
                                     <input class="form-control" type="text" name="b"
                                      value="{{$question->b}}">

                                       @if ($errors->first('b'))
                                        <div class="alert alert-danger ">
                                           {{$errors->first('b')}}
                                        </div>
                                      @endif
                                 </div>

                                 <div class="form-group">
                                     <label>C</label>
                                     <input class="form-control" type="text" name="c"
                                      value="{{$question->c}}">

                                      @if ($errors->first('c'))
                                        <div class="alert alert-danger ">
                                           {{$errors->first('c')}}
                                        </div>
                                      @endif

                                 </div>

                                 <div class="form-group">
                                     <label>D</label>
                                     <input class="form-control" type="text" name="d"
                                      value="{{$question->d}}">

                                      @if ($errors->first('d'))
                                        <div class="alert alert-danger ">
                                           {{$errors->first('d')}}
                                        </div>
                                      @endif

                                 </div>

                                 <div class="form-group">
                                     <label>Answer</label>
                                     <input class="form-control" type="text" name="answer"
                                      value="{{$question->answer}}">

                                      @if ($errors->first('answer'))
                                        <div class="alert alert-danger ">
                                           {{$errors->first('answer')}}
                                        </div>
                                      @endif

                                 </div>

                                  <div class="form-group">
                                      <label>Modules</label>
                                      <select class="form-control" type="text" name="module_id" >
                                         <option></option>
                                      @foreach($module as $module)
                                         <option value="{{$module->id}}" {{$question->module_id == $module->id ? 'selected="selected"' : ' '}}>{{$module->name}}</option>
                                      @endforeach
                                     </select>

                                     @if ($errors->first('module_id'))
                                        <div class="alert alert-danger ">
                                           {{$errors->first('module_id')}}
                                        </div>
                                      @endif
                                  </div>



                                 <input type="submit" value="New Question Edit" class="btn btn-success"  role="button">

                                 <a href="/questions" class="btn btn-success"  role="button">Come back Question's Home</a>
                         </form>
                </div>

            </div>
    </div>
@endsection


