@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">

      <h1>Questions</h1>
            <table  class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Question</th>
                        <th>A</th>
                        <th>B</th>
                        <th>C</th>
                        <th>D</th>
                        <th>Answer</th>
                        <th>Module</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($question as $question )

                        <tr>

                             <td>{{$question->text}}</td>
                             @if($question->a  == $question->answer || $question->answer == 'a')
                                <td class="bg-success"> {{$question->a}}</td>
                             @elseif($question->a  != $question->answer)
                                 <td class="bg-danger"> {{$question->a}}</td>
                             @endif

                             @if($question->b  == $question->answer || $question->answer == 'b')
                                <td class="bg-success"> {{$question->b}}</td>
                             @elseif($question->b  != $question->answer)
                                 <td class="bg-danger"> {{$question->b}}</td>
                             @endif

                            @if($question->c  == $question->answer || $question->answer == 'c')
                                <td class="bg-success"> {{$question->c}}</td>
                             @elseif($question->c  != $question->answer)
                                 <td class="bg-danger"> {{$question->c}}</td>
                             @endif

                              @if($question->d  == $question->answer || $question->answer == 'd')
                                <td class="bg-success"> {{$question->d}}</td>
                             @elseif($question->d  != $question->answer)
                                 <td class="bg-danger"> {{$question->d}}</td>
                             @endif

                             <td>{{$question->answer}}</td>
                             <td>{{$question->module->name}}</td>
                             <td><a  href="/questions/<?php echo $question->id ?>" class="btn btn-success"  role="button" >See</a></td>
                             <td><a  href="/questions/<?php echo $question->id ?>/edit" class="btn btn-success"  role="button" >Edit</a></td>
                             <td>
                                <form method="post" action="/questions/{{$question->id}}">
                                  {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="delete">
                                    <input type="submit" value="Destroy" class="btn btn-danger"  role="button">
                                </form>
                             </td>


                         </tr>

                    @endforeach
                     <a  href="/questions/create" class="btn btn-success"  role="button" >Create</a>
                </tbody>
            </table>

         </div>
    </div>
</div>
@endsection
