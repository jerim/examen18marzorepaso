@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Modules</h1>

            <table  class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Code</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($module as $module )

                        <tr>
                             <td>{{$module->name}}</td>
                             <td>{{$module->code}}</td>

                             @can('view',$module)
                             <td><a  href="/modules/<?php echo $module->id ?>" class="btn btn-success"  role="button" >See</a></td>
                             @else

                             <td>Don't look</td>
                             @endcan

                             <td><a  href="/modules/<?php echo $module->id ?>/edit" class="btn btn-success"  role="button" >Edit</a></td>

                             <td>
                                <form method="post" action="/modules/{{$module->id}}">
                                     {{ csrf_field() }}
                                     <input type="hidden" name="_method" value="delete">
                                     <input type="submit" value="Destroy" class="btn btn-danger"  role="button">
                              </form>
                             </td>


                         </tr>

                    @endforeach
                       <a  href="/modules/create" class="btn btn-success"  role="button" >Create</a>
                </tbody>
            </table>

         </div>
    </div>
</div>
@endsection
