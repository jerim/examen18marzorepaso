@extends('layouts.app')

@section('title', 'Module')

@section('content')
            <h1>
               Module's show's list <?php echo $module->id ?>
            </h1>

            <ul>
                <li>Name: {{$module->name}} </li>
            </ul>

             <h2>
              Exam's list <?php echo $module->id ?>
             </h2>

             <ul>
                @foreach($module->exams as $exam )
                <li> Exam: {{$exam->title}}</li>
                @endforeach
             </ul>


            <a href="/modules" class="btn btn-success"  role="button">Come back Modules's Home</a>


@endsection

