@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Families</h1>

      <table  class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Name</th>
            <th>Code</th>
          </tr>
        </thead>

        <tbody>
          @foreach($family as $family )

          <tr>
           <td>{{$family->name}}</td>
           <td>{{$family->code}}</td>
           <td><a  href="/families/<?php echo $family->id ?>" class="btn btn-success"  role="button" >See</a></td>
           <td><a  href="/families/<?php echo $family->id ?>/edit" class="btn btn-success"  role="button" >Edit</a></td>
           <td>
            <form method="post" action="/families/{{$family->id}}">
             {{ csrf_field() }}
             <input type="hidden" name="_method" value="delete">
             <input type="submit" value="Destroy" class="btn btn-danger"  role="button">
           </form>
         </td>
       </tr>

       @endforeach
       <a  href="/families/create" class="btn btn-success"  role="button" >Create</a>
     </tbody>
   </table>

 </div>
</div>
</div>
@endsection
