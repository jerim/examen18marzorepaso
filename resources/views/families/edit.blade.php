@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h1>Edit Families</h1>
            <form class="form"  method="post" action="/families/{{$family->id}}">
                {{ csrf_field() }}

                 <input type="hidden" name="_method" value="put">

                <div class="form-group">
                    <label>Name</label>
                    <input class="form-control" type="text" name="name"
                    value="{{$family->name}}">

                    @if ($errors->first('name'))
                    <div class="alert alert-danger ">
                        {{$errors->first('name')}}
                    </div>
                    @endif


                </div>

                <div class="form-group">
                    <label>Code</label>
                    <input class="form-control" type="text" name="code"
                     value="{{$family->code}}">


                    @if ($errors->first('code'))
                    <div class="alert alert-danger ">
                        {{$errors->first('code')}}
                    </div>
                    @endif

                </div>



                <input type="submit" value="New Edit Families" class="btn btn-success"  role="button">

                <a href="/families" class="btn btn-success"  role="button">Come back Familie's Home</a>
            </form>
        </div>

    </div>
</div>
@endsection


