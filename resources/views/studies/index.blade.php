@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Studies</h1>

      <table  class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Name</th>
            <th>Code</th>
            <th>Family</th>
          </tr>
        </thead>

        <tbody>
          @foreach($study as $study )

          <tr>
           <td>{{$study->name}}</td>
           <td>{{$study->code}}</td>
           <td>{{$study->family->name}}</td>
           <td><a  href="/studies/<?php echo $study->id ?>" class="btn btn-success"  role="button" >See</a></td>
           <td><a  href="/studies/<?php echo $study->id ?>/edit" class="btn btn-success"  role="button" >Edit</a></td>
           <td>
            <form method="post" action="/studies/{{$study->id}}">
             {{ csrf_field() }}
             <input type="hidden" name="_method" value="delete">
             <input type="submit" value="Destroy" class="btn btn-danger"  role="button">
           </form>
         </td>
       </td>
     </tr>

     @endforeach
     <a  href="/studies/create" class="btn btn-success"  role="button" >Create</a>
   </tbody>
 </table>

</div>
</div>
</div>
@endsection
