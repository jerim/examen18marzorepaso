@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h1>Create Studies</h1>
            <form class="form"  method="post" action="/studies">
                {{ csrf_field() }}

                <div class="form-group">
                    <label>Name</label>
                    <input class="form-control" type="text" name="name">

                    @if ($errors->first('name'))
                    <div class="alert alert-danger ">
                        {{$errors->first('name')}}
                    </div>
                    @endif


                </div>

                <div class="form-group">
                    <label>Code</label>
                    <input class="form-control" type="text" name="code">

                    @if ($errors->first('code'))
                    <div class="alert alert-danger ">
                        {{$errors->first('code')}}
                    </div>
                    @endif

                </div>

                <div class="form-group">
                    <label>Family</label>
                    <select class="form-control" type="text" name="family_id">
                        <option></option>
                        @foreach($family as $family)
                        <option value="{{$family->id}}"{{old('family_id') == $family->id ? 'selected="selected"' : ' '}}>{{$family->name}}</option>
                        @endforeach
                    </select>

                    @if ($errors->first('family_id'))
                    <div class="alert alert-danger ">
                     {{$errors->first('family_id')}}
                 </div>
                 @endif
             </div>




             <input type="submit" value="New Studies" class="btn btn-success"  role="button">

             <a href="/studies" class="btn btn-success"  role="button">Come back Studie's Home</a>
         </form>
     </div>

 </div>
</div>
@endsection


