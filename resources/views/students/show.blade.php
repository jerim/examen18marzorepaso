@extends('layouts.app')

@section('title', 'Students')

@section('content')
            <h1>
               Student's show's list <?php echo $student->id ?>
            </h1>

            <ul>
                <li>Name: {{$student->name}} </li>
                <li>Surname: {{$student->surname}}</li>
                <li>Date: {{$student->date}}</li>
                <li>Address: {{$student->address}} </li>
                <li>Email: {{$student->email}}</li>

            </ul>



            <a href="/students" class="btn btn-success"  role="button">Come back Student's Home</a>


@endsection

