@extends('layouts.app')

@section('content')
    <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                        <h1>Create Students</h1>
                            <form class="form"  method="post" action="/students">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label>Name</label>
                                    <input class="form-control" type="text" name="name" value="{{old('name')}}">

                                    @if ($errors->first('name'))
                                    <div class="alert alert-danger ">
                                        {{$errors->first('name')}}
                                    </div>
                                    @endif

                                </div>
                                <div class="form-group">
                                    <label>Surname</label>
                                    <input class="form-control" type="text" name="surname" value="{{old('surname')}}">

                                    @if ($errors->first('surname'))
                                    <div class="alert alert-danger ">
                                        {{$errors->first('surname')}}
                                    </div>
                                    @endif

                                </div>

                                <div class="form-group">
                                    <label>Date</label>
                                    <input class="form-control" type="date" name="date" value="{{old('date')}}">

                                     @if ($errors->first('date'))
                                    <div class="alert alert-danger ">
                                        {{$errors->first('date')}}
                                    </div>
                                    @endif

                                </div>

                                 <div class="form-group">
                                    <label>Address</label>
                                    <input class="form-control" type="text" name="address" value="{{old('address')}}">

                                    @if ($errors->first('address'))
                                    <div class="alert alert-danger ">
                                        {{$errors->first('address')}}
                                    </div>
                                    @endif

                                </div>


                                  <div class="form-group">
                                    <label>Email</label>
                                    <input class="form-control" type="text" name="email" value="{{old('email')}}">

                                    @if ($errors->first('email'))
                                    <div class="alert alert-danger ">
                                        {{$errors->first('email')}}
                                    </div>
                                    @endif

                                </div>



                                 <input type="submit" value="New Student" class="btn btn-success"  role="button">
                         </form>
                </div>

            </div>
    </div>
@endsection


