@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Students</h1>
            <table  class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Date</th>
                        <th>Address</th>
                        <th>Email</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($student as $student )

                        <tr>
                             <td>{{$student->name}}</td>
                             <td>{{$student->surname}}</td>
                             <td>{{$student->date}}</td>
                             <td>{{$student->address}}</td>
                             <td>{{$student->email}} </td>
                             <td><a  href="/students/<?php echo $student->id ?>" class="btn btn-success"  role="button" >See</a></td>
                             <td><a  href="/students/<?php echo $student->id ?>/edit" class="btn btn-success"  role="button" >Edit</a></td>
                             <td>
                              <form method="post" action="/students/{{$student->id}}">
                                  {{ csrf_field() }}
                                 <input type="hidden" name="_method" value="delete">
                                 <input type="submit" value="Destroy" class="btn btn-danger"  role="button">
                              </form>
                             </td>


                         </tr>

                    @endforeach
                        <a  href="/students/create" class="btn btn-success"  role="button" >Create</a>

                </tbody>
            </table>

         </div>
    </div>
</div>
@endsection
