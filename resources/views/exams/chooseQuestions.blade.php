@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Choose Question's Exam's </h1>
      @if(Session::has('module'))
      Module: {{Session::get('module')->name }}
      <a  href="/exams/back" class="btn btn-success"  role="button" >back modules</a>
      @endif

      <h4>Up's info's of exam</h4>
      <h4>Module: {{$module->name}}</h4>
      <h4>Title:{{Session::get('title')}} </h4>
      <h4>Date:{{ date('d-m-Y', strtotime(Session::get('date'))) }}</h4>


      <h3>Questions of Exam's</h3>
      <ul>
        @if (Session::has('questions'))
        @foreach(Session::get('questions') as $question)
        <li>{{ $question->text }} </li>
        @endforeach
        @endif
      </ul>



      <table  class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Module</th>
            <th>Question</th>
            <th>A</th>
            <th>B</th>
            <th>C</th>
            <th>D</th>
            <th>Answer</th>

          </tr>
        </thead>

        <tbody>
          @foreach($questions as $question )

          <tr>
           <td>{{$question->module->name}}</td>
           <td>{{$question->text}}</td>
           <td>{{$question->a}}</td>
           <td>{{$question->b}}</td>
           <td>{{$question->c}}</td>
           <td>{{$question->d}}</td>
           <td>{{$question->answer}}</td>

           <td>
            @if (isset(Session::get('questions')[$question->id]))
            <a  href="/exams/<?php echo $question->id ?>/minus" class="btn btn-danger"  role="button" >-</a>
            @else
            <a  href="/exams/<?php echo $question->id ?>/plus" class="btn btn-success"  role="button" >+</a>
            @endif
          </td>
        </tr>

        @endforeach
        <a  href="/exams/save" class="btn btn-success"  role="button" >Save exam</a>
        <a  href="/exams/trash" class="btn btn-danger"   role="button" >Reset exam</a>
      </tbody>
    </table>

  </div>
</div>
</div>
@endsection
