@extends('layouts.app')

@section('title', 'Exams')

@section('content')
            <h1>
               Exam's show's list <?php echo $exam->id ?>
            </h1>

            <ul>
                <li>Title: {{$exam->title}} </li>
                <li>Date: {{$exam->date}}</li>
                <li>User:{{$exam->user->name}}</li>

            </ul>

            <h2>
              Question's list <?php echo $exam->id ?>
            </h2>
              <ul>
                @foreach($exam->questions as $question)
                <li> Question {{$question->text}}</li>
                @endforeach
              </ul>


            <a href="/exams" class="btn btn-success"  role="button">Come back Exams's Home</a>


@endsection

