@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Exams</h1>
      <a  href="/exams/create" class="btn btn-success"  role="button" >Create</a>
      <a  href="/exams/new" class="btn btn-success"  role="button" >new exam</a>
      @if(Session::has('exam'))
      Exam's remember : {{Session::get('exam')->title }}
      <a  href="/exams/forget" class="btn btn-success"  role="button" >Forget</a>
      @endif

      @if(Session::has('exam'))


      <a  href="/exams/trash" class="btn btn-danger"  role="button" >destroy</a>

      @endif

      <table  class="table table-striped table-hover">
        <thead>
            <tr>
                <th>Title</th>
                <th>Date</th>
                <th>Module</th>
                <th>User</th>
            </tr>
        </thead>

        <tbody>
            @foreach($exam as $exam )

            <tr>
               <td>{{$exam->title}}</td>
               <td>{{$exam->date}}</td>
               <td>{{$exam->module->name}}</td>
               <td>{{$exam->user->name}}</td>
               <td><a  href="/exams/<?php echo $exam->id ?>" class="btn btn-success"  role="button" >See</a></td>
               <td><a  href="/exams/<?php echo $exam->id ?>/edit" class="btn btn-success"  role="button" >Edit</a></td>
               <td><a  href="/exams/<?php echo $exam->id ?>/remember" class="btn btn-success"  role="button" >Remember</a></td>
               <td>
                  <form method="post" action="/exams/{{$exam->id}}">
                      {{ csrf_field() }}

                      <input type="hidden" name="_method" value="delete">
                      <input type="submit" value="Destroy" class="btn btn-danger"  role="button">

                  </form>
              </td>


          </tr>

          @endforeach


      </tbody>
  </table>

</div>
</div>
</div>
@endsection
