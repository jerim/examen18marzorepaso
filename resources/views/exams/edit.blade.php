@extends('layouts.app')

@section('content')
    <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                        <h1>Edit Exams</h1>
                            <form class="form"  method="post" action="/exams/{{$exam->id}}">
                                {{ csrf_field() }}

                                 <input type="hidden" name="_method" value="put">

                                <div class="form-group">
                                    <label>Title</label>
                                    <input class="form-control" type="text" name="title" value="{{$exam->title}}">

                                     @if ($errors->first('title'))
                                        <div class="alert alert-danger ">
                                            {{$errors->first('title')}}
                                        </div>
                                    @endif


                                </div>

                                <div class="form-group">
                                    <label>Date</label>
                                    <input class="form-control" type="date" name="date" value="{{$exam->date}}">

                                     @if ($errors->first('date'))
                                    <div class="alert alert-danger ">
                                        {{$errors->first('date')}}
                                    </div>
                                    @endif


                                </div>


                                <div class="form-group">
                                    <label>Modules</label>
                                    <select class="form-control" type="text" name="module_id" >
                                        <option></option>
                                    @foreach($module as $module)
                                        <option value="{{$module->id}}" {{$exam->module_id == $module->id ? 'selected="selected"' : ' '}}>{{$module->name}}</option>
                                    @endforeach
                                    </select>

                                     @if ($errors->first('module_id'))
                                    <div class="alert alert-danger ">
                                        {{$errors->first('module_id')}}
                                    </div>
                                    @endif


                                </div>



                                 <input type="submit" value="New Exam Edit" class="btn btn-success"  role="button">

                                 <a href="/exams" class="btn btn-success"  role="button">Come back Exam's Home</a>
                         </form>
                </div>

            </div>
    </div>
@endsection


