<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/exams/forget', 'ExamController@forget');
Route::get('/exams/{id}/remember', 'ExamController@remember');
Route::get('/exams/new', 'ExamController@new');
Route::post('/exams/setModule', 'ExamController@setModule');

Route::get('/exams/chooseQuestions', 'ExamController@chooseQuestions');
Route::get('/exams/back', 'ExamController@back');
Route::get('/exams/{id}/plus', 'ExamController@plus');
Route::get('/exams/save', 'ExamController@save');
Route::get('/exams/trash', 'ExamController@trash');
Route::get('/exams/{id}/minus', 'ExamController@minus');
Route::get('/exams/trash', 'ExamController@trash');


Route::resource('/exams', 'ExamController');
Route::resource('/questions', 'QuestionController');
Route::resource('/users', 'UserController');
Route::resource('/modules', 'ModuleController');
Route::resource('/students', 'StudentController');
Route::resource('/families', 'FamilyController');
Route::resource('/studies', 'StudyController');

//attach y detach

Route::post('/studies/{id}/modules',  'StudyController@attachModule');
Route::delete('/studies/{id}/modules', 'StudyController@detachModule');




//shows
Route::get('users/{id}', function ($id) {
   return "Users show's list $id";
});

Route::get('exams/{id}', function ($id) {
   return "Exams show's list $id";
});

Route::get('modules/{id}', function ($id) {
   return "Modules show's list $id";
});

Route::get('families/{id}', function ($id) {
   return "families show's list $id";
});

Route::get('studies/{id}', function ($id) {
   return "Studies show's list $id";
});

